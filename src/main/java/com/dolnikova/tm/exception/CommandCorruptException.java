package com.dolnikova.tm.exception;

public final class CommandCorruptException extends Exception {

    public CommandCorruptException() {
        super("Command doesn't exist");
    }

}
