package com.dolnikova.tm.api.bootstrap;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.service.DtoService;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull IUserService getUserService();
    @NotNull Collection<AbstractCommand> getCommands();
    @NotNull DtoService getDtoService();
}
