package com.dolnikova.tm.api.service;

import java.util.Comparator;

public interface Comparable<E> {

    Comparator<E> getStatusComparator(final boolean direction);
    Comparator<E> getCreationDateComparator(final boolean direction);
    Comparator<E> getStartDateComparator(final boolean direction);
    Comparator<E> getEndDateComparator(final boolean direction);

}
