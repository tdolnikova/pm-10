package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import org.jetbrains.annotations.NotNull;

public final class UserSignOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.USER_SIGN_OUT;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.USER_SIGN_OUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getUserService().setCurrentUser(null);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
