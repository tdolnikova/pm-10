package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.util.DateUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class TaskPersistCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.PERSIST_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.PERSIST_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final Project project = findProject();
        if (project == null) return;
        System.out.println(Constant.INSERT_TASK);
        boolean taskCreationCompleted = false;
        while (!taskCreationCompleted) {
            @NotNull final String taskText = Bootstrap.scanner.nextLine();
            if (taskText.isEmpty()) taskCreationCompleted = true;
            else {
                @Nullable final Task newTask = new Task(serviceLocator.getUserService().getCurrentUser().getId(), taskText);
                newTask.setProjectId(project.getId());
                System.out.println(Constant.INSERT_START_DATE);
                boolean dateChosen = false;
                while (!dateChosen) {
                    @NotNull String startDate = Bootstrap.scanner.nextLine();
                    if (startDate.isEmpty()) return;
                    newTask.setStartDate(DateUtil.stringToDate(startDate));
                    dateChosen = true;
                }
                System.out.println(Constant.INSERT_END_DATE);
                dateChosen = false;
                while (!dateChosen) {
                    @NotNull final String endDate = Bootstrap.scanner.nextLine();
                    if (endDate.isEmpty()) return;
                    newTask.setEndDate(DateUtil.stringToDate(endDate));
                    dateChosen = true;
                }
                serviceLocator.getTaskService().persist(serviceLocator.getUserService().getCurrentUser().getId(), newTask);
                System.out.println(Constant.TASK + " " + taskText + " " + Constant.CREATED_F);
            }
        }
        System.out.println(Constant.TASK_ADDITION_COMPLETED);
    }

    @Nullable
    private Project findProject() {
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectService().findOneById(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
