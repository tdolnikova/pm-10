package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.FIND_ALL_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.FIND_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if(!isSecure()) return;
        @Nullable final List<Task> allTasks = serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        if (allTasks.isEmpty()) System.out.println(Constant.NO_TASKS);
        else {
            for (final Task task : allTasks) {
                System.out.println(task.getName());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
