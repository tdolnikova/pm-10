package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import org.jetbrains.annotations.NotNull;


public final class TaskRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.REMOVE_ALL_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.REMOVE_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getTaskService().removeAll(serviceLocator.getUserService().getCurrentUser().getId());
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
