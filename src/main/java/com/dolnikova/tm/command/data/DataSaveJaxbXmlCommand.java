package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.Objects;

public final class DataSaveJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Constant.DATA_SAVE_JAXB_XML;
    }

    @Override
    public @NotNull String description() {
        return Constant.DATA_SAVE_JAXB_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String ownerId = serviceLocator.getUserService().getCurrentUser().getId();
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.setUsers(serviceLocator.getUserService().findAll(ownerId));
        dto.setProjects(serviceLocator.getProjectService().findAll(ownerId));
        dto.setTasks(serviceLocator.getTaskService().findAll(ownerId));
        serviceLocator.getDtoService().saveJaxbXml(dto);
        System.out.println("СОХРАНЕНО");
    }

    @Override
    public boolean isSecure() {
        assert serviceLocator != null;
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}