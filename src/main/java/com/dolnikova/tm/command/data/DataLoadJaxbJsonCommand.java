package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataLoadJaxbJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Constant.DATA_LOAD_JAXB_JSON;
    }

    @Override
    public @NotNull String description() {
        return Constant.DATA_LOAD_JAXB_JSON_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final EntityDTO dto = serviceLocator.getDtoService().loadJaxbJson();
        @Nullable final String ownerId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().persistList(ownerId, dto.getProjects());
        serviceLocator.getTaskService().persistList(ownerId, dto.getTasks());
        serviceLocator.getUserService().persistList(ownerId, dto.getUsers());
        System.out.println("ЗАГРУЖЕНО");
    }

    @Override
    public boolean isSecure() {
        assert serviceLocator != null;
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}