package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataSaveBinCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Constant.DATA_SAVE_BIN;
    }

    @Override
    public @NotNull String description() {
        return Constant.DATA_SAVE_BIN_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String ownerId = serviceLocator.getUserService().getCurrentUser().getId();
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.setUsers(serviceLocator.getUserService().findAll(ownerId));
        dto.setProjects(serviceLocator.getProjectService().findAll(ownerId));
        dto.setTasks(serviceLocator.getTaskService().findAll(ownerId));
        serviceLocator.getDtoService().saveBin(dto);
        System.out.println("СОХРАНЕНО");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }

}
