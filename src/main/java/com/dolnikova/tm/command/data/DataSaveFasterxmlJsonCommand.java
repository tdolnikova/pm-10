package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;

public final class DataSaveFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Constant.DATA_SAVE_FASTERXML_JSON;
    }

    @Override
    public @NotNull String description() {
        return Constant.DATA_SAVE_FASTERXML_JSON_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String ownerId = serviceLocator.getUserService().getCurrentUser().getId();
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.setUsers(serviceLocator.getUserService().findAll(ownerId));
        dto.setProjects(serviceLocator.getProjectService().findAll(ownerId));
        dto.setTasks(serviceLocator.getTaskService().findAll(ownerId));
        serviceLocator.getDtoService().saveFasterxmlJson(dto);
        System.out.println("СОХРАНЕНО");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
