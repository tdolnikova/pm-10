package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

public final class DataLoadFasterxmlXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Constant.DATA_LOAD_FASTERXML_XML;
    }

    @Override
    public @NotNull String description() {
        return Constant.DATA_LOAD_FASTERXML_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final EntityDTO dto = serviceLocator.getDtoService().loadFasterxmlXml();
        @Nullable final String ownerId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().persistList(ownerId, dto.getProjects());
        serviceLocator.getTaskService().persistList(ownerId, dto.getTasks());
        serviceLocator.getUserService().persistList(ownerId, dto.getUsers());
        System.out.println("ЗАГРУЖЕНО");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }

}