package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.SortingType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.SORT_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.SORT_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        if (projectList == null || projectList.isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.CHOOSE_SORTING_TYPE + ":");
        for(final SortingType type : SortingType.values()) {
            System.out.println(type.displayName());
        }
        boolean typeChosen = false;
        while (!typeChosen) {
            @NotNull final String userChoice = Bootstrap.scanner.nextLine();
            if (userChoice.isEmpty()) return;
            if (userChoice.equals(SortingType.BY_STATUS.displayName())){
                typeChosen = true;
                Collections.sort(projectList, serviceLocator.getProjectService().getStatusComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_CREATION_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, serviceLocator.getProjectService().getCreationDateComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_START_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, serviceLocator.getProjectService().getStartDateComparator(true));
            }
            else if (userChoice.equals(SortingType.BY_END_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, serviceLocator.getProjectService().getEndDateComparator(true));
            }
            else {
                System.out.println(Constant.TRY_AGAIN);
            }
        }
        for (final Project project : projectList) {
            System.out.println(project.getId());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
