package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.MERGE_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.MERGE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_PROJECT_NAME);
        @Nullable Project foundProject = null;
        while (foundProject == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            foundProject = serviceLocator.getProjectService().findOneById(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (foundProject == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + Constant.TRY_AGAIN);
            else {
                System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
                boolean newNameInserted = false;
                while (!newNameInserted) {
                    @NotNull final String newData = Bootstrap.scanner.nextLine();
                    if (newData.isEmpty()) return;
                    serviceLocator.getProjectService().merge(serviceLocator.getUserService().getCurrentUser().getId(), newData, foundProject, DataType.NAME);
                    System.out.println(Constant.PROJECT_UPDATED);
                    newNameInserted = true;
                }
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
