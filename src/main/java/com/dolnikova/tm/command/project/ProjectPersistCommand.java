package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.util.DateUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectPersistCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Constant.PERSIST_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Constant.PERSIST_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
        @NotNull final String projectName = Bootstrap.scanner.nextLine();
        if (projectName.isEmpty()) return;
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
        if (project != null) {
            System.out.println(Constant.PROJECT_NAME_ALREADY_EXIST);
            return;
        }
        @Nullable final Project newProject = new Project(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
        System.out.println(Constant.INSERT_START_DATE);
        boolean dateChosen = false;
        while (!dateChosen) {
            @NotNull final String startDate = Bootstrap.scanner.nextLine();
            if (startDate.isEmpty()) return;
            newProject.setStartDate(DateUtil.stringToDate(startDate));
            dateChosen = true;
        }
        System.out.println(Constant.INSERT_END_DATE);
        dateChosen = false;
        while (!dateChosen) {
            @NotNull final String endDate = Bootstrap.scanner.nextLine();
            if (endDate.isEmpty()) return;
            newProject.setEndDate(DateUtil.stringToDate(endDate));
            dateChosen = true;
        }
        serviceLocator.getProjectService().persist(serviceLocator.getUserService().getCurrentUser().getId(), newProject);
        System.out.println(Constant.PROJECT + " " + projectName + " " + Constant.CREATED_M);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
