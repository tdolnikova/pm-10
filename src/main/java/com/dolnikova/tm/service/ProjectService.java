package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(ownerId, id);
    }

    @Override
    public Project findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return projectRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<Project> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<Project> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String ownerId, @Nullable final Project entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        projectRepository.persist(ownerId, entity);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final Project entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        projectRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final Project entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        projectRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        projectRepository.removeAll(ownerId);
    }

    @Nullable
    @Override
    public Comparator<Project> getStatusComparator(boolean direction) {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    @Nullable
    @Override
    public Comparator<Project> getCreationDateComparator(boolean direction) {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getCreationDate().compareTo(o2.getCreationDate());
            }
        };
    }

    @Nullable
    @Override
    public Comparator<Project> getStartDateComparator(boolean direction) {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        };
    }

    @Nullable
    @Override
    public Comparator<Project> getEndDateComparator(boolean direction) {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getEndDate().compareTo(o2.getEndDate());
            }
        };
    }

}
