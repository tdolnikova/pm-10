package com.dolnikova.tm.service;

import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.EntityDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@NoArgsConstructor
public class DtoService {

    private final static String DATA_BIN = "data.bin";
    private final static String DATA_JSON = "data.json";
    private final static String DATA_XML = "data.xml";

    public void saveBin(EntityDTO dto) throws Exception {
        @Nullable final File path = new File(Constant.SAVE_PATH + DATA_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(dto);
        }
    }

    public void saveFasterxmlJson(EntityDTO dto) throws Exception {
        @Nullable final File path = new File(Constant.SAVE_PATH + DATA_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, dto);
    }

    public void saveFasterxmlXml(EntityDTO dto) throws Exception {
        @Nullable final File path = new File(Constant.SAVE_PATH + DATA_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, dto);
    }

    public void saveJaxbJson(EntityDTO dto) throws Exception {
        @Nullable final File path = new File(Constant.SAVE_PATH + DATA_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dto, path);
    }

    public void saveJaxbXml(EntityDTO dto) throws Exception {
        @Nullable final File path = new File(Constant.SAVE_PATH + DATA_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dto, path);
    }

    @Nullable
    public EntityDTO loadBin() throws Exception {
        @Nullable final File file = new File(Constant.SAVE_PATH + DATA_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final EntityDTO dto = (EntityDTO) objectInputStream.readObject();
            return dto;
        }
    }

    @Nullable
    public EntityDTO loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(Constant.SAVE_PATH + DATA_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final EntityDTO dto = objectMapper.readValue(file, EntityDTO.class);
        return dto;
    }

    @Nullable
    public EntityDTO loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(Constant.SAVE_PATH + DATA_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final EntityDTO dto = xmlMapper.readValue(file, EntityDTO.class);
        return dto;
    }

    @Nullable
    public EntityDTO loadJaxbJson() throws Exception {
        @Nullable final File file = new File(Constant.SAVE_PATH + DATA_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final EntityDTO dto = (EntityDTO) unmarshaller.unmarshal(file);
        return dto;
    }

    @Nullable
    public EntityDTO loadJaxbXml() throws Exception {
        @Nullable final File file = new File(Constant.SAVE_PATH + DATA_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final EntityDTO dto = (EntityDTO) unmarshaller.unmarshal(file);
        return dto;
    }

}
